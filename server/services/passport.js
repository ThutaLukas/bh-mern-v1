const passport = require('passport');
const GoogleStrategy  = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const keys = require('../config/index');
const {User} = require('../models/User')


passport.use(

    new FacebookStrategy({
        clientID : keys.FACEBOOK_APP_ID ,
        clientSecret : keys.FACEBOOK_APP_SECRET ,
        callbackURL :  `/auth/facebook/callback` ,
        profileFields : ['emails' , 'displayName' , 'id'] ,
        proxy : true 

    } ,

    async (accessToken , refreshToken , profile , done ) => {

        const existingUser =   await User.findOne({'facebook.id': profile.id}) ;

                if(existingUser){
                    // we already has UserId , send it back
                    return done(null , existingUser );
            }
    
            const user =   await new User({
                            
                'facebook.id' : profile.id ,
                'facebook.username' : profile.displayName ,
                'facebook.email' : profile.emails[0].value 
               
                

            }).save();
                    
             done(null , user );

    }
    
    )



)