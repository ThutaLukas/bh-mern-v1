const passport = require('passport');
const express = require('express');
const userRoutes = require('./api/user')
const cookieParser = require('cookie-parser')
const authRoutes = require('./api/auth')

module.exports = function (app) {
    // third party middlewares 
    app.use(express.json()) 
    app.use(express.urlencoded({extended : true }))
    app.use(passport.initialize())
    app.use(cookieParser());


    // api middlewares 

    app.use('/api/v1/users' , userRoutes) 
    app.use('/auth' , authRoutes) 

}