const {User} = require('../models/User');
const normailizeErrors = require('../services/mongoose')

exports.register = function (req , res ) {

    let {email , password , username} = req.body;

    if(!email || !password) {
           return res.status(422).send({errors: [{title: 'Data missing!', detail: 'Provide email and password!'}]});
    }

    const user = new User({

             local : {
                 username : username ,
                 email : email ,
                 password : password
             }

    })

        user.save((err , userDoc ) => {
            if(err) {return  res.json({success : false , errors : normailizeErrors(err.errors)  })}

            res.status(500).json({success : true })

        })
}






exports.login = function (req , res ) {


    User.findOne({ 'local.email' : req.body.email } , (err , user ) => {
            if(!user) {
                return res.json({loginSuccess : false , message : 'Auth Failed , user not found '})
            }

            user.comparePassword(req.body.password , (err , isMatch) => {
                if(!isMatch) {
                    return res.json({loginSuccess : false , message : 'Wrong password'})
                }

                
                user.generateToken((err , user) => {
                    if(err) {return res.status(400).send(err)}

                    res.cookie('auth' , user.token , {httpOnly : false  , maxAge :365 * 24 * 60 * 60 * 1000 } ).status(200).json({loginSuccess : true })


                })

            })


    })



}

exports.auth =  function (req , res ) {


        if(!req.user.facebook) {
            res.status(200).json({
                email : req.user.local.email ,
                username : req.user.local.username,
                isAuth : true ,
                authType : 'local' ,
                role : req.user.role
                
   
           })

        }else {

            res.status(200).json({
                email : req.user.facebook.email ,
                username : req.user.facebook.username,
                isAuth : true ,
                authType : 'facebook',
                role : req.user.role
                
   
           })
        }
      




}


exports.logout = function ( req , res ) {

        User.findOneAndUpdate( {_id : req.user._id} , {token : ''} , (err , updateUser) => {
              
            
            if(err) {return res.json({success : false , err })}

                return res.json(200).send({success : true })
        })
            



}