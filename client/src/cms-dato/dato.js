import {SiteClient} from 'datocms-client';


class Dato {

    constructor() {
        this.client = new SiteClient('YOUR_DATO_API_KEY')
    }



    getPages  = () => {
        this.client.items.all();
    }

    getPage = ( id ) => {
        this.client.items.find(id)
    }

    getType = (type) => {
        this.client.items.all({'filter[type]' : type })
    }

}

export default new Dato();