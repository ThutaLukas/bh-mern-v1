import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form';
import userReducer from './userReducer'
import { reducer as toastrReducer} from 'react-redux-toastr'
import async from '../components/async/asyncReducer'
import asyncReducer from '../components/async/asyncReducer';
import modalReducer from '../components/utils/modalManager/modalReducer'



export const reducers = combineReducers ({
        form : formReducer ,
        user : userReducer ,
        toastr : toastrReducer ,
        async : asyncReducer ,
        modal : modalReducer
})