
import React from 'react'
import formInput from '../../utils/form/formInput'
import formError from '../../utils/form/ErrorForm';
import {reduxForm , Field} from 'redux-form'





 const Login =  (props) => {
   
   const {handleSubmit , pristine , submitting , submitCb , valid} = props;
  
   
    return (
        <form onSubmit={handleSubmit(submitCb)}>
                <Field 
                    name="email"
                    type="text"
                    className="form-control"
                    label="email"
                    component={formInput}
                />

                 <Field
                        name="password"
                        type="password"
                        label='Password'
                        className='form-control'
                        component={formInput}
                    />
                <button type="submit" className="btn btn-block btn-primary"
                 disabled={!valid || pristine || submitting }
                >
                Login
                
                </button>
 
        </form>
    )
}


export default reduxForm({
    form : 'userLogin'
})(Login)