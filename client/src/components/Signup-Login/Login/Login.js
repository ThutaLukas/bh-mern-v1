import React, { Component } from 'react'
import {connect} from 'react-redux'
import LoginForm from './LoginForm'
import { loginUser } from '../../../actions/userActions'
import { withRouter , Link} from 'react-router-dom'


class Login extends Component {
  
   constructor() {
    super();

    this.state = {
        errors : []
    }

   }
  
  
   loginUserHandler = (userData) => {

        this.props.dispatch(loginUser(userData))
          .then((response) => {
                
                if(response.payload.loginSuccess) {
                       
                        this.props.history.push('/user/dashboard')
                }
                      

                   

          })


   }


    render() {
  
  
  
  
       return (
         
            <section className="py-5 my-5">
                    <div className="container">
                            <div className="row text-center">
                                    <div className="col-12">
                                            <LoginForm submitCb={this.loginUserHandler}/>
                                            <a href="/auth/facebook" className="btn btn-primary mt-5 text-white"> <i className="fa fa-facebook-square mx-3"></i>Login with Facebook</a>
                                            <Link to="/" className="btn btn-primary mt-5 ">Back to Home</Link>
                                    </div>

                                    
                            </div>
                        
                    </div>    
            </section>
    )
  }
}

// const mapStateToProps = (state) => ({
  
// })




export default  connect()( withRouter(Login)) ;