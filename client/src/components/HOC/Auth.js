
    import React, { Component } from 'react';
    import { connect } from 'react-redux';
    import { auth } from '../../actions/userActions';
    import isEmpty from '../../utils/isEmpty'
    import {withRouter} from 'react-router-dom'
    

    export default function(ComposedClass,reload,adminRoute = null){
       
       
        class AuthenticationCheck extends Component {
    
            // state = {
            //     loading: true
            // }
    
            
            componentDidMount(){
                
                this.props.dispatch(auth()).then(response =>{
                    let user = this.props.user.userData;
    
                    if(!user.isAuth){
                        if(reload){
                            this.props.history.push('/')
                        }
                       


                    } else{
                      
                            if(reload === false){
                                this.props.history.push('/user/dashboard')
                            }
                        
                    }
                    // this.setState({loading:false})
                })
                      
               
               
            }
    
    
            render() {
              

                // if(this.state.loading){
                //     return (
                //         <div className="text-center py-5 my-5">
                //             <CircularProgress style={{color:'#2196F3'}} thickness={7}/> 
                //         </div>
                //     )
                // }
                return (
                       <ComposedClass {...this.props} user={this.props.user}/>
                );
            }
        }
    
         
        function mapStateToProps(state){
                return {
                    user: state.user
                }
            }

       
        
      
        
    
        return connect(mapStateToProps)(AuthenticationCheck)
    }
    
