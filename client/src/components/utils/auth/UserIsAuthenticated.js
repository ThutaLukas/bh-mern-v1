import { connectedReduxRedirect } from 'redux-auth-wrapper/history4/redirect';



export const UserIsAuthenticated = connectedReduxRedirect({
    wrapperDisplayName : 'UserIsAuthenticated' ,
    allowRedirectBack : true ,
    redirectPath : '/' ,
    authenticatedSelector : ({user}) => user.userData.isAuth && !user.userData.error
   
})