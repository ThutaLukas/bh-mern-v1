
import React from 'react'

export default (props) => {
   
   const errors = props.errors;
   
    return (
        errors.length > 0  && 
        <div className='alert alert-danger'>
           {errors.map((error, index) => <p key={index}> {error.detail} </p>)}
         </div>
    )
}
