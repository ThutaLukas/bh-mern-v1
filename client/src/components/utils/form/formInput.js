import React from 'react'





 const formInput = (
    {
       label , 
       input ,
       type ,
       symbol ,
       className ,
       meta : { touched , error , warning } 
    }
 ) => {
 
 
 
 
 
    return (
      <div className="form-group">
         <label>{label}</label>

         <div className="input-group">
         
          {symbol && 
            <div class="input-group-prepend">
                <div className="input-group-text">{symbol}</div>
            </div>
        
          }
          <input {...input} type={type} className={className}/>

         </div>
         {touched &&
         ((error && <div className='alert alert-danger'>{error}</div>))
        }
      </div>
  )
}

 
export default formInput;